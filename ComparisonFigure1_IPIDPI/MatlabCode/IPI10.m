%% Parameters 
% define the true system and the weighting matrix 
A=[1.01,0.01,0;0.01,1.01,0.01;0,0.01,1.01]; % system matrix A
B=eye(3); % input matrix B
Q=0.001*eye(3); % weighting matrix Q
R=eye(3); % weighting matrix R
% some parameters about policy iteration 
episode=50; % total number of episodes
Num=10;% \tau_IPI
K_initial=diag([-1.5,-1,-0.5]); % initial gain K_0
[Pstar,Kstar,L,info]=idare(A,B,Q,R); % K^* and P^* from DARE
xstart=[30;20;10]; % initial state x_0
%% Initialization of RLS
nx=size(A,2); % number of states
nu=size(B,2); % number of control inputs
datalength=episode*(Num);
Ahistory=zeros(nx*nx,datalength);
Bhistory=zeros(nx*nu,datalength);
A_est=0*eye(3); % initial \hat{A}_0
B_est=0*eye(3); % initial \hat{B}_0
PK=100*eye(6);  % initial matrix PK=H_t^{-1}
%% iterations of PE and PI(basic parameters)
xhistory=zeros(nx,datalength);
uhistory=zeros(nu,datalength);
khistory=zeros(nx*nu,episode);
xhistory(:,1)=xstart;
khistory(:,1)=K_initial(:);
K_current=K_initial;
phistory=zeros(nx*nx,episode);
factor=1; % additional excitation
%% Start Algorithm
for j=1:episode
    %% Policy Evaluation
    Pmb=dlyap((A_est+B_est*K_current)',Q+K_current'*R*K_current); % policy evalutaion based on estiamtes (\hat{A}_{i-1},\hat{B}_{i-1})
    phistory(:,j)=Pmb(:);
    %% RLS
    for i=1:Num
        uforward=[factor*randn(1,1);factor*randn(1,1);factor*randn(1,1)]; % e_t from Gaussian
        u=uforward+K_current*xhistory(:,(j-1)*Num+i); 
        uhistory(:,(j-1)*Num+i)=u;
        xhistory(:,(j-1)*Num+i+1)=realsystem(A,B,xhistory(:,(j-1)*Num+i),u); % Collect the data d_t
        [A_est,B_est,PK]=RLS(A_est,B_est,PK,xhistory(:,(j-1)*Num+i),xhistory(:,(j-1)*Num+i+1),u); % RLS
        Ahistory(:,(j-1)*Num+i)=A_est(:);
        Bhistory(:,(j-1)*Num+i)=B_est(:);
    end
    %% Policy improvement
    khistory(:,j)=vec(K_current);    
    K_current=-(R+B_est'*Pmb*B_est)\B_est'*Pmb*A_est;  % policy improvement based on estiamtes (\hat{A}_{i},\hat{B}_{i})
end
subplot(2,1,1);
plot(Normalized_data(phistory,Pstar),'linewidth',1,'color','green');
xlabel('$\mathrm{iteration~index}$','interpreter','latex','FontSize',12);
ylabel('$\frac{\|\mathbf{P}-\mathbf{\hat{P}}\|}{\|\mathbf{\hat{P}}\|}$','interpreter','latex','FontSize',12);
subplot(2,1,2)
plot(Normalized_data(khistory,-Kstar),'linewidth',1,'color','green');
xlabel('$\mathrm{iteration~index}$','interpreter','latex','FontSize',12);
ylabel('$\frac{\|\mathbf{K}-\mathbf{\hat{K}}\|}{\|\mathbf{\hat{K}}\|}$','interpreter','latex','FontSize',12);
%% required function
% real system dynamic
function xnew=realsystem(A,B,state,controlinput)
    xnew=A*state+B*controlinput;
end

