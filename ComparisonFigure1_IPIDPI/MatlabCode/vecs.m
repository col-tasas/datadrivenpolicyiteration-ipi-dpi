function output=vecs(input)
% stack the upper-triangular part of symmetric matrix P
    [a,b]=size(input);
    output=zeros(a*(b+1)/2,1);
    for i=1:a
        output((2*a+2-i)*(i-1)/2+1:(2*a+2-i)*(i-1)/2+a-i+1,1)=input(i:end,i);
    end
end