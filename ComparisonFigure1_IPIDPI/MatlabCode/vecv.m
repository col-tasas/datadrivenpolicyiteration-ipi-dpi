function output=vecv(input)
% vecv(v)=[v_1^2,2v_1v_2,...,2v_1v_n,v_2^2,...,2v_2v_n,...,v_n^2]^T rearranges the entries of vector $v$ in a specific pattern
    a=numel(input);
    output=zeros(a*(a+1)/2,1);
    for i=1:a
        for j=1:a-i+1
            if j==1
                output((2*a+2-i)*(i-1)/2+1:(2*a+2-i)*(i-1)/2+1,1)=input(i)^2;
            else
                output((2*a+2-i)*(i-1)/2+j:(2*a+2-i)*(i-1)/2+1+j,1)=2*input(i)*input(i+j-1);
            end
        end
    end
end