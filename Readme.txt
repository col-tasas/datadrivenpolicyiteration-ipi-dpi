Dear all,

The code is for the work titled “The Role of Identification in Data-driven Policy Iteration: A System Theoretic Study”. 

For the Figure 1:

Please choose the Folder ComparisonFigure1_IPIDPI then MatlabCode, and run the code IPI1.m, IPI5.m, IPI10.m, IPI10_nonPE.m, and DPI.m, save the data, and then you can reproduce the same figure. All the required matrix operation (vecs, inv_vecs, vecv, vec) functions are provided! The Matlab data files in the Folder named FigureData are used to plot the figure in our work.

For the Figure 2:

Please choose the Folder ComparisonFigure2_IPIDPIPG then MatlabCode, and run the code IPI.m, DPI.m, RLS_PG_Gamma0001, and RLS_PG_Gamma001, save the data, and then you can reproduce the same figure. The Matlab data files in the Folder named FigureData are used to plot the figure in our work.

Thanks for your visit! If you have any questions related to the code, please contact: bowen.song@ist.uni-stuttgart.de

Best regards,
Bowen Song 