function output=inv_vecs(input)
% the inverse operation of vecs
    a=(-1+sqrt(1+8*numel(input)))/2;
    output=zeros(a,a);
    num=0;
    for i=a:-1:1
        output(a-i+1:end,a-i+1)=input(num+1:num+i);
        output(a-i+1,a-i+1:end)=input(num+1:num+i)';
        num=num+i;
    end
end