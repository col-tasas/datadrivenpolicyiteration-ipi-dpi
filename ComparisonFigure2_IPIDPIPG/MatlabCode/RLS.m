function [A,B,P_new]=RLS(A_old,B_old,P_old,x_old,x_new,u)
%% Inputs:
% A_old B_old: previous system dynamic
% P_old: H_{t-1}^{-1}
% x_old x_new u: x_t,x_{t+1},u_t
%% Outputs:
% A B: updated estimated system dynamic
% P_new: H_t^{-1}
    d=[x_old;u];
    P_new=P_old-P_old*(d)*d'*P_old/(1+d'*P_old*d); % rank 1 update: as stated in Remark. 2
    EST=[A_old,B_old];
    EST_new=EST+(x_new-EST*d)*d'*P_new;
    [a,~]=size(A_old);
    A=EST_new(:,1:a);
    B=EST_new(:,a+1:end);
end