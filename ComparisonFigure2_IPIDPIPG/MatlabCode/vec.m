function output=vec(input)
% function vec: vectorize a matrix
    output=input(:);
end