function figuredata = Normalized_data(data1,ref)
    data2=kron(ones(1,size(data1,2)),vec(ref));
    figuredata=(sqrt(sum((data1-data2).*(data1-data2),1)))/sqrt(sum(ref(:).*ref(:)));
end