%% Parameters 
% define the true system and the weighting matrix 
A=[-0.53,0.42,-0.44;0.42,-0.56,-0.65;-0.44,-0.65,0.35]; % system matrix A
B=[0.43,-0.82;0.53,-0.78;0.26,0.40]; % input matrix B
Q=[6.12,1.72,0.53;1.72,6.86,1.72;0.53,1.72,5.73]; % weighting matrix Q
R=[1.15,-0.23;-0.23,3.62]; % weighting matrix R
% some parameters about policy iteration 
episode=100; % total number of episodes
Num=12;% \tau_IPI
[~,K_initial,~,~]=idare(A,B,100*Q,R); % initial gain K_0
[Pstar,Kstar,L,info]=idare(A,B,Q,R); % K^* and P^* from DARE
xstart=[9;7;5]; % initial state x_0
%% iterations of PE and PI(basic parameters)
nx=size(A,2); % number of states
nu=size(B,2); % number of control inputs
datalength=episode*Num; 
xhistory=zeros(nx,datalength);
uhistory=zeros(nu,datalength);
khistory=zeros(nx*nu,episode);
xhistory(:,1)=xstart;
x_current=xstart;
K_initial=-K_initial;
khistory(:,1)=K_initial(:);
K_current=K_initial; 
phistory=zeros(nx*nx,episode);
factor=3; % additioanl excitation(Gaussian distributed signal)
for j=1:episode
    %% Policy Evaluation
    for i=1:Num/2  
        xhistory(:,(j-1)*Num+2*i-1)=x_current;
        uforward=[factor*randn(1,1);factor*randn(1,1)]; % \eta_t generation
        u1=uforward+K_current*x_current;
        uhistory(:,(j-1)*Num+2*i-1)=u1;
        x_current=realsystem(A,B,x_current,u1);
        xhistory(:,(j-1)*Num+2*i)=x_current;
        u2=-uforward+K_current*x_current; % \eta_{t+1}=-\eta_t to form the data pairs
        uhistory(:,(j-1)*Num+2*i)=u2;
        x_current=realsystem(A,B,x_current,u2);
    end
    xhistory(:,(j-1)*Num+2*i+1)=x_current;
    sum1=zeros(nx*(nx+1)/2,nx*(nx+1)/2);
    sum2=zeros(nx*(nx+1)/2,1);
    stagecost=zeros(1,Num/2);
    for i=1:Num/2
        stagecost(:,i)=com_stagecost(xhistory(:,(j-1)*Num+i*2-1)+xhistory(:,(j-1)*Num+i*2),K_current*(xhistory(:,(j-1)*Num+i*2-1)+xhistory(:,(j-1)*Num+i*2)),Q,R); % r_k 
        phik=vecv(xhistory(:,(j-1)*Num+2*i-1)+xhistory(:,(j-1)*Num+2*i))-vecv(xhistory(:,(j-1)*Num+2*i)+xhistory(:,(j-1)*Num+2*i+1)); % \psi_k in Eq.(46)
        sum1=sum1+(phik)*(phik)';
        sum2=sum2+(phik)*stagecost(1,i);
    end
    P=inv_vecs((sum1)\sum2); % Eq.(47) to compute \hat{P}_i
    phistory(:,j)=P(:);
    khistory(:,j)=vec(K_current);
    %% Policy Improvement
    sum1=zeros(nx*nu+nu*(nu+1)/2,1);
    sum2=zeros(nx*nu+nu*(nu+1)/2,nx*nu+nu*(nu+1)/2);
    for i=1:Num
        ct=xhistory(:,(j-1)*Num+i)'*(Q+K_current'*R*K_current-P)*xhistory(:,(j-1)*Num+i)+xhistory(:,(j-1)*Num+i+1)'*P*xhistory(:,(j-1)*Num+i+1);
        rt=[kron(2*xhistory(:,(j-1)*Num+i),uhistory(:,(j-1)*Num+i)-K_current*xhistory(:,(j-1)*Num+i));vecv(uhistory(:,(j-1)*Num+i))-vecv(K_current*xhistory(:,(j-1)*Num+i))]; % Gamma_t in Eq.(39)
        sum1=sum1+rt*ct;
        sum2=sum2+rt*rt';
    end
    delta=(sum2)\sum1; % Eq.(41)
    BPA=reshape(delta(1:nx*nu,:),nu,nx); % B^TP_iA
    BPB=inv_vecs(delta(nx*nu+1:end)); 
    K_current=-inv(BPB+R)*BPA; % Policy improvement 
end
%% Plot the figure
subplot(2,1,1);
plot(Normalized_data(phistory,Pstar),'linewidth',1,'color','red');
hold on
xlabel('$\mathrm{iteration~index}$','interpreter','latex','FontSize',12);
ylabel('$\frac{\|\mathbf{P}-\mathbf{\hat{P}}\|_F}{\|\mathbf{\hat{P}}\|_F}$','interpreter','latex','FontSize',12);
subplot(2,1,2)
plot(Normalized_data(khistory,-Kstar),'linewidth',1,'color','red');
xlabel('$\mathrm{iteration~index}$','interpreter','latex','FontSize',12);
ylabel('$\frac{\|\mathbf{K}-\mathbf{\hat{K}}\|_F}{\|\mathbf{\hat{K}}\|_F}$','interpreter','latex','FontSize',12);
%% required function
function xnew=realsystem(A,B, state,controlinput) % system dynamic
    xnew=A*state+B*controlinput;
end
function stagecost=com_stagecost(xstate,ustate,Q,R) % funtion to compute the stage cost
    stagecost=xstate'*Q*xstate+ustate'*R*ustate;
end