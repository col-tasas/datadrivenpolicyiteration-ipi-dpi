function figuredata = Normfigure2(data1,data2,ref)
    %figuredata=(sqrt(sum(data1.*data1,1))-sqrt(sum(data2.*data2,1)))/norm(ref(:));
    figuredata=(sqrt(sum((data1-data2).*(data1-data2),1)))/norm(ref(:));
end