%% Parameters  
% define the true system and the weighting matrix 
A=[-0.53,0.42,-0.44;0.42,-0.56,-0.65;-0.44,-0.65,0.35]; % system matrix A
B=[0.43,-0.82;0.53,-0.78;0.26,0.40]; % input matrix B
Q=[6.12,1.72,0.53;1.72,6.86,1.72;0.53,1.72,5.73]; % weighting matrix Q
R=[1.15,-0.23;-0.23,3.62]; % weighting matrix R
% some parameters about policy iteration 
episode=1000; % total number of episodes
Num=1;% \tau_IPI
[~,K_initial,~,~]=idare(A,B,100*Q,R); % initial gain K_0
[Pstar,Kstar,L,info]=idare(A,B,Q,R); % K^* and P^* from DARE
xstart=[9;7;5]; % initial state x_0
%% Initialization of RLS
nx=size(A,2); % number of states
nu=size(B,2); % number of control inputs
datalength=episode*(Num);
Ahistory=zeros(nx*nx,datalength);
Bhistory=zeros(nx*nu,datalength);
A_est=zeros(nx,nx); % initial \hat{A}_0
B_est=zeros(nx,nu); % initial \hat{B}_0
PK=1000*eye(nx+nu);  % initial matrix PK=H_t^{-1}
%% iterations of PE and PI(basic parameters)
xhistory=zeros(nx,datalength);
uhistory=zeros(nu,datalength);
khistory=zeros(nx*nu,episode);
xhistory(:,1)=xstart;
K_initial=-K_initial;
khistory(:,1)=K_initial(:);
K_current=K_initial;
phistory=zeros(nx*nx,episode);
factor=3; % additional excitation
%% Start Algorithm
for j=1:episode
    j
    %% RLS
    for i=1:Num
        uforward=[factor*randn(1,1);factor*randn(1,1)]; % e_t from Gaussian
        u=uforward+K_current*xhistory(:,(j-1)*Num+i); 
        uhistory(:,(j-1)*Num+i)=u;
        xhistory(:,(j-1)*Num+i+1)=realsystem(A,B,xhistory(:,(j-1)*Num+i),u); % Collect the data d_t
        [A_est,B_est,PK]=RLS(A_est,B_est,PK,xhistory(:,(j-1)*Num+i),xhistory(:,(j-1)*Num+i+1),u); % RLS
        Ahistory(:,(j-1)*Num+i)=A_est(:);
        Bhistory(:,(j-1)*Num+i)=B_est(:);
    end
    %% Policy Gradient
    khistory(:,j)=vec(K_current);   
    Pmb=dlyap((A_est+B_est*K_current)',Q+K_current'*R*K_current); % based on estiamtes (\hat{A}_{i-1},\hat{B}_{i-1})
    phistory(:,j)=Pmb(:);
    W=dlyap((A_est+B_est*K_current),eye(nx));
    K_new=K_current-0.001*(R*K_current+B_est'*Pmb*(A_est+B_est*K_current))*W;  % policy optimization based on estiamtes (\hat{A}_{i},\hat{B}_{i}) \gamma can be chosen here.
    K_current=K_new;

end
subplot(2,1,1);
plot(Normalized_data(phistory,Pstar),'linewidth',1,'color','green');
xlabel('$\mathrm{iteration~index}$','interpreter','latex','FontSize',12);
ylabel('$\frac{\|\mathbf{P}-\mathbf{\hat{P}}\|}{\|\mathbf{\hat{P}}\|}$','interpreter','latex','FontSize',12);
subplot(2,1,2)
plot(Normalized_data(khistory,-Kstar),'linewidth',1,'color','green');
xlabel('$\mathrm{iteration~index}$','interpreter','latex','FontSize',12);
ylabel('$\frac{\|\mathbf{K}-\mathbf{\hat{K}}\|}{\|\mathbf{\hat{K}}\|}$','interpreter','latex','FontSize',12);
%% required function
% real system dynamic
function xnew=realsystem(A,B,state,controlinput)
    xnew=A*state+B*controlinput;
end

